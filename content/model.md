# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en bokförd post och varje kolumn motsvarar en egenskap för den beskrivna bokförda posten. 5 attribut är definierade, där alla 5 är obligatoriska. 


<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**account**](#account)|1|[heltal](#heltal)|**Obligatoriskt** -  Kontonummer enligt kommun-bas 23.|
|[**operation**](#operation)|0..1|[heltal](#heltal)|**Rekommenderad** - Verksamhetskod enligt SCBs instruktioner.|
|[**counterparty**](#counterparty)|0..1|[heltal](#heltal)|**Rekommenderad** - Motpart enligt kommun-bas 23.|
|[**category**](#type)|0..1|P &vert; D|**Rekommenderad** - Uppmärkning för att hålla isär drift och investeringar.|
|[**amount**](#amount)|1|[decimal](#decimal)|**Obligatoriskt** - Belopp i kronor. Debet ger positiva värden och kredit ger negativa värden.|


</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

</div>

## Förtydligande av attribut

### account

Reguljärt uttryck: **`/^\-?\\d+$/`**

Anger konton enligt kommun-bas 23 där X = valfri siffra. Tillåtna värden anges enligt terminologin [Kommunbas-23](https://dataportal.se/concepts/kommunbas-23) 



### operation

Reguljärt uttryck: **`/^\-?\\d+$/`**

Verksamhetskoder enligt SCBs instruktioner, där X = valfri siffra. Tillåtna värden anges enligt terminologin [Verksamhetskoder](https://dataportal.se/concepts/verksamhetskoder) 


### counterparty

Reguljärt uttryck: **`/^\-?\\d+$/`**

Motpartskoder enligt SCBs instruktioner, där X = valfri siffra. Tillåtna värden anges i tabellen nedan.
<div class="ms_datatable">

| Motpart	| Motpartsnamn      |
| -- | :---------------:|
|1XX|Kommuninterna enheter|
|2XX|Kommuninterna enheter|
|3XX|Kommuninterna enheter|
|4XX|Kommuninterna enheter|
|5XX|Kommunägda företag|
|6XX|Kommunägda företag|
|7XX|Kommunägda företag|
|81X|Staten och staliga myndigheter|
|811|Skolverket|
|812|Socialstyrelsen|
|813|Migrationsverket|
|814|Arbetsförmedlingen|
|815|Försäkringskassan|
|82X|Kommuner|
|83X|Kommunalförbund och SKR|
|84X|Regioner|
|85X|Föreningar och stiftelser|
|86X|Enskilda personer och hushåll|
|87X|Privata företag|
|91X|Företag och organisationer inom länder inom EU|
|92X|Organisationen EU|
|95X|Företag och organisationer inom länder utanför EU|



### category

Anger uppmärkning för att hålla isär drift och investeringar. Märks upp på rader med konto 
3XXX - 7XXX och 852X interna motparter avseende kalkylerad kapitalkostnad. Värden som kan anges:
<div>
Värden som kan anges:
<div>
D = Drift 
<div>
P = Investeringsprojekt
<div>
Kan lämnas tom på rader med konto 1XXX - 2XXX och 8XXX förutom 852X interna motparter avseende kalkylerad kapitalkostnad.


### amount

Reguljärt uttryck: **`/^[-+]?\d*\.?\d+$/`** 

Anger belopp i kronor. Debet ger positiva värden och kredit ger negativa värden. Exempelvärden som kan anges: 

-10000
<div>
20000
<div>



 




