**Bakgrund**

Denna specifikation beskriver kommunala bokföringsuppgifter som standardiserats i enlighet med den relevanta årsversionen av baskontoplanen för kommuner (Kommun-BAS) och den relevanta versionen av den verksamhetsindelning som används av Statistiska centralbyrån.

Specifikationen ger kommuner i Sverige möjligheten att dela med sig av sina bokföringsuppgifter på ett jämförbart sätt.

För frågor som rör uppgifter som publiceras enligt specifikationen, vänligen kontakta aktuell kommun. För frågor som rör Kommun-BAS, vänligen kontakta organisationen Sveriges kommuner och regioner. För frågor som rör verksamhetsindelning, vänligen kontakta Statistiska centralbyrån.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>

**[SCB](https://www.scb.se/)** - Första datamodell och arbetsgrupp <br>



