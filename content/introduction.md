# Introduktion 

Denna specifikation anger vilka fält som är obligatoriska för kommunala bokföringsuppgifter. 5 attribut kan anges varav 2 är obligatoriska. 

I appendix A finns ett exempel på hur kommunala bokföringsuppgifter uttrycks i CSV. I appendix B uttrycks samma exempel i JSON. 

Denna specifikation definierar en enkel tabulär informationsmodell för kommunala bokföringsuppgifter. Specifikationen innefattar också en beskrivning av hur informationen uttrycks i formaten CSV och JSON. Som grund förutsätts CSV kunna levereras då det är ett praktiskt format och skapar förutsägbarhet för mottagare av informationen. 
