# Exempel i JSON

Samma entitet som i Appendix A, fast nu som JSON.

```json
{
    "account":  1920,
    "operation": 8456,
    "counterparty": 871,
    "category":  "D",
    "amount":  -100000,     
}
```
